+++
categories = ["category"]
description = "Register for the next Make.Better."
date = "2018-06-09T00:10:00Z"
title = "Register"
author = ""
draft = false

+++

Purchase tickets for the next Make.Better. Symposium.
The cost is US$16 or JM$2,000.

<!-- Ticket Tailor Widget. Paste this in to your website where you want the widget to appear. Do no change the code or the widget may not work properly. -->
<div class="tt-widget"><div class="tt-widget-fallback"><p><a href="https://www.tickettailor.com/new-order/174270/2c47/ref/website_widget/" target="_blank">Click here to buy tickets</a><br /><small><a href="http://www.tickettailor.com?rf=wdg" class="tt-widget-powered">Sell tickets online with Ticket Tailor</a></small></p></div><script src="https://dc161a0a89fedd6639c9-03787a0970cd749432e2a6d3b34c55df.ssl.cf3.rackcdn.com/tt-widget.js" data-url="https://www.tickettailor.com/new-order/174270/2c47/ref/website_widget/" data-type="inline" data-inline-minimal="true" data-inline-show-logo="false" data-inline-bg-fill="true"></script></div>
<!-- End of Ticket Tailor Widget -->

+++
summary = "A symposium series for software practitioners"
date = "2017-03-11T14:59:49-05:00"
title = "About"

+++

Make. Better. is a symposium series for software practitioners who make software and strive to make better software. The first symposium in the series was held at Startup Jamaica on March 24, 2017.  The next one will be held on March 28, 2019.
<!--more-->
This is an opportunity to connect with the local community of developers and, through shared experiences, improve the way we build software. Join fellow craftsmen as we share the processes, tools and techniques that help to make better software.  

## What to expect at a Symposium
Our symposiums serve as a signal that the local community of practice is raising the bar. We want to see local software developers making a global impact. Make. Better. symposiums cover best practices and case studies on software testing, quality assurance, agile development and project management. Presentations are made by members of the local and wider software development community. Additionally, we encourage break-out sessions to facilitate small group discussion on topics of interest.

## Who should attend?

Do you relish the challenge and effort involved in making software better? Want to improve the quality of software produced locally?  This is the place for you. The symposium is open to persons involved in all aspects of the software development lifecycle, including product managers, UX practitioners, software developers, testers, educators, CTOs, CIOs,  scientists, engineers and students.





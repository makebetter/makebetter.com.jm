+++
author = ""
date = "2018-06-09T13:04:33Z"
title = "Make.Better. March 2018"
draft = false
tags = ["tag1","tag2"]
categories = ["category"]
description = ""

+++

The March edition of the *Make.Better.* Symposium was held on Thursday March 29, 2018 at The Hub CoWorking Space.
Developers and others who have an interest in the software development industry gathered to exchange knowledge and share battle stories. 
Once again, we had representation from various software shops based here in Kingston, Jamaica. We had two main presentations and discussion on topics important to the craft.

In addition to our short talks we added 3 lightning talk slots. 

![Attendees at the March 29, 2018 Make.Better. Symposium](/img/1803/MakeBetterAudience.jpg)

## Talks

![Gebre Wallace at at the March 29, 2018 Make.Better. Symposium](/img/1803/GebreWallacePresents.jpg)

Gebre Wallace discussed "agile vs waterfall" and why, in real life, you may need a hybrid of the two approaches.




![Khary Sharpe at at the March 29, 2018 Make.Better. Symposium](/img/1803/KharySharpPresentingMain.jpg)

Khary Sharpe introduced us to the tools for building mobile applications with Vue.js and NativeScript.

![Tremaine Buchanan at the March 29, 2018 Make.Better. Symposium](/img/1803/TremaineBuchananPresents.jpg)
Tremaine Buchanan presented his perspective on working with a Microsoft based stack after years of open source development.

## Lightning Talks

![Matthew McNaughton at the March 29, 2018 Make.Better. Symposium](/img/1803/MatthewMcNaughtonPresents.jpg)

Matthew McNaughton from the SlashRoots Foundation walked us through the implications of the Data Protection Act.

![Jordan Jones at the March 29, 2018 Make.Better. Symposium](/img/1803/JordanJonesPresents.jpg)
Jordan Jones introduced us to the peer to peer [Beaker webbrowser](https://beakerbrowser.com/).

![Dmitri Dawkins at the March 29, 2018 Make.Better. Symposium](/img/1803/DmitriDawkinsPresents.jpg)
Dmitri Dawkins of the Branson Centre provide good perspective on funding opportunities for business ventures.

## Next Make.Better.

The next *Make.Better.* event is scheduled for June 28, 2018. [Tickets are available online](https://makebetter.com.jm/register) 

+++
author = ""
draft = false
tags = ["make better","origin"]
categories = ["background"]
description = ""
date = "2017-03-12T22:00:35-05:00"
title = "What does Make.Better. mean?"

+++

It is our hope that through the *Make.Better* symposiums, participants will be encouraged to improve both their processes and their products.
<!--more -->
The name is a play on the word "make", it is a call to become a better "maker" of software but it is also a call to "make" better software. 

The name also embraces the concept of continuous improvement as a way of thinking. As a software craftsman, feel free to make *Make.Better.* your motto.
....


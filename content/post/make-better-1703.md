+++
categories = ["event"]
description = ""
author = "David Bain"
date = "2017-06-16T08:22:17-05:00"
title = "Make.Better. the first Software Craftsmanship symposium. Kingston, Jamaica"
draft = false
tags = ["software craftsmanship","event"]

+++

The first *Make.Better.* Symposium was held on Friday March 24, 2017 at Startup Jamaica. Our enthusiastic participants represented various software shops based here in Kingston, Jamaica. We had two main presentations and discussion on topics important to the craft.

![Attendees at the March 23, 2017 Make.Better. Symposium](/img/1703/makebetterattendees1703.jpg)

The first presentation was by Solomon Perkins and was an introduction to automating testing using the Robot Framework
![Solomon Perkins](/img/1703/solomonperkins-presenting.jpg)

Egbert Frankberg of KnightFox Apps shared some of the practices that they use to be more productive with Jira for project management. This covered burn down charts and integration with Slack.
![Egbert](/img/1703/egbert.jpg)



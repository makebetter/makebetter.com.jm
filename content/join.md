+++
date = "2017-03-11T18:25:48-05:00"
title = "Join"
description = ""
author = ""
draft = false
tags = ["tag1","tag2"]
categories = ["category"]

+++
<style>
#form-iframe{ 
    width: 90%;
    height: 2900px;
    border: 0px solid transparent;
}
</style>
Make.Better. is a symposium series held in March, June and September of each year.
Signing the form below will allow us to be in touch with you about participating in
the series.
The cost per event is USD$20/JMD$2500. The (June 30, 2017) event is free.

<iframe id="form-iframe" src="https://docs.google.com/forms/d/e/1FAIpQLSfoPo7OPjT8XYhrXFQ6fe4jLEEoDh4EicYpR8K6SxdS5iKWZg/viewform?embedded=true&usp=embed_googleplus"></iframe>

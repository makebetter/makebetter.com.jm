+++
summary = "Prospectus for the Make.Better. symposium series"
date = "2017-03-11T14:59:49-05:00"
title = "Prospectus"

+++

You can support our events through sponsorship or endorsement. We have provided a prospectus with more information. 

<a class="button text-link" href="https://docs.google.com/document/d/1aKPisL80XX1FaZdyVHz3SqIS_5Jpxtk8WNM7f_fm0iY/preview">View the prospectus</a>


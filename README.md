# Overview
This site is a hugo site.
Deployment is powered by gitlab CI and https://bitbucket.org/alteroo/hugo-and-firebase
To update the site just commit to master.
This will trigger a CI job which will push to the live site (hosting on firebase).

## Todo
If you want to know what we need to fix next check out the `todo.txt` file

## Getting Hugo
Because we have a gitlab pipeline you don't have to have Hugo to work with the site but
if you plan to post new content it is recommended.
Visit https://gohugo.io to get hugo.

# Updating content
Top level pages are created in the `content` folder and
New posts are created in the `content/post` folder.

Use the following commands:

## New top level page:

    hugo new my-page.md

Then you can edit at:

    content/my-page.md

## New post:

    hugo new post/post-name.md

Then you can edit it at:

    content/post/post-name.md

# Managing the front page
The front page is part of the theme
most commonly we update the hero at:

    themes/makebetter/layouts/partials/hero.html 

# Managing the navigation menu
The nav menu is part of the theme
To edit it, open:

    themes/makebetter/layouts/partials/nav.html

## Publishing 
To publish it run the following command:

    hugo undraft path/to/content.md
    eg.
       hugo undraft content/post/what-do-you-mean-by-make-better.md 


# Deployment
Deployment is as simple as pushing your changes to master.
The deployment details are managed in the `.gitlab-ci.yml` file.


# Changes
The docker image for deployment now uses a newer version of node and firebase (FROM node:8.14.0-alpine)

